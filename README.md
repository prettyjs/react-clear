### Installation from scratch to fresh and clean Ubuntu 16.04 (VirtualBox)

## Pre

- VirtualBox and Ubuntu 16.04
- Git

```
~ sudo apt-get install git
```

## Clone project from BitBucket
 
```
~ git clone https://username@bitbucket.org/legko777/lgkone.git
~ git clone https://username@bitbucket.org/legko777/lgkone_engine.git
```

## Installs

- NodeJs

```
~ sudo apt-get install nodejs
```

- npm

```
~ sudo apt-get install npm
```

- nvm (nodeJs version manager for changing nodeJs version without headache)

[nvm on github](#https://github.com/creationix/nvm)

```
~ wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
```

After installation close and open terminal window

- install specific nodejs version (6.11.2 using in project)

```
~ nvm install 6.11.2
~ nvm alias default v6.11.2
```

- Global packages

```
~ npm install -g hiredis
~ npm install -g make
~ npm install -g coffee-script
~ sudo apt install redis-server
```

after that run redis

## Database

- mysql

```
~ sudo apt-get install mysql-server
```

It will require root password

```
~ sudo apt-get install mysql-client
```

After installation open mysql by command `~ mysql -u root -p` and create
new user

```
~ CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
~ GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';
~ FLUSH PRIVILEGES;
```

After that, you need to create `/config.json` file depends on 
`./config.json.sample` in project with your
username and password to connect to mysql databases.

Create databases in mysql
```
mysql: CREATE DATABASE lgk1_dev
mysql: CREATE DATABASE lgk1_queue_dev
mysql: CREATE DATABASE lgk1_auth_dev
```

## Bitcoind

- install bitcoind

[instruction source](https://gist.github.com/rjmacarthy/b56497a81a6497bfabb1)

Add repository and install bitcoind

```
`sudo apt-get install build-essential`
`sudo apt-get install libtool autotools-dev autoconf`
`sudo apt-get install libssl-dev`
`sudo apt-get install libboost-all-dev`
`sudo add-apt-repository ppa:bitcoin/bitcoin`
`sudo apt-get update`
`sudo apt-get install bitcoind`
`mkdir ~/.bitcoin/ && cd ~/.bitcoin/`
`nano bitcoind.conf`
```

Add config to bitcoin.conf file

```
rpcuser=username
rpcpassword=password
testnet=1
rpcport=8332
rpcallowip=127.0.0.1
server=1
```

Start bitcoind

```
bitcoind --daemon
```

Attention! There is a possibility, that bitcoind don't use conffile --
you can set username and password manually (todo: run in dev mode 
without cpu usage)

```
bitcoind --daemon -rpcpassword=password -rpcuser=username
```

## installs

- npm install in lgk1 and lgk1_engine

```
~ npm install
```

- create tables with cake in lgk1 and lgk1_engine

```
~ cake
```

it will list all commands

```
~ cake db:create_tables
~ cake db:seed_market_stats
~ cake db:seed_trade_stats
```

## run

- run in lgk1
```
node app.js
node core_api.js
node queue.js
```

- run in lgk1_engine

```
node engine.js
```