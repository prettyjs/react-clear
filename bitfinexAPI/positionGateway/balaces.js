// view how to authenticate here:
// https://docs.bitfinex.com/v1/docs/rest-auth

var payload = {
    "request": "/v1/balances",
    "nonce": Date.now().toString()
  }
//   [{
//     "type":"deposit",
//     "currency":"btc",
//     "amount":"0.0",
//     "available":"0.0"
//   },{
//     "type":"deposit",
//     "currency":"usd",
//     "amount":"1.0",
//     "available":"1.0"
//   },{
//     "type":"exchange",
//     "currency":"btc",
//     "amount":"1",
//     "available":"1"
//   },{
//     "type":"exchange",
//     "currency":"usd",
//     "amount":"1",
//     "available":"1"
//   },{
//     "type":"trading",
//     "currency":"btc",
//     "amount":"1",
//     "available":"1"
//   },{
//     "type":"trading",
//     "currency":"usd",
//     "amount":"1",
//     "available":"1"
//   },
//   ...]