// view how to authenticate here:
// https://docs.bitfinex.com/v1/docs/rest-auth

var payload = {
    request: '/v1/mytrades',
    nonce: Date.now().toString(),
    currency: 'USD'
 }
 
 // or use bitfinex-api-node
 
 const BFX = require('bitfinex-api-node')
 const bfxRest = new BFX(apiKey, apiSecretKey, {version: 1}).rest
 
 bfxRest.past_trades('USD', {} , (err, res) => {
   if (err) console.log(err)
   console.log(result)
 })

 
//  [{
//     "price":"246.94",
//     "amount":"1.0",
//     "timestamp":"1444141857.0",
//     "exchange":"",
//     "type":"Buy",
//     "fee_currency":"USD",
//     "fee_amount":"-0.49388",
//     "tid":11970839,
//     "order_id":446913929
//   }]