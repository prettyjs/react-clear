// view how to authenticate here:
// https://docs.bitfinex.com/v1/docs/rest-auth

var payload = {
  request: '/v1/mytrades',
  nonce: Date.now().toString(),
  currency: 'USD'
}

//  [{
//     "price":"246.94",      /*********************
//     "amount":"1.0",        /***********************
//     "timestamp":"1444141857.0",
//     "exchange":"",
//     "type":"Buy",
//     "fee_currency":"USD",
//     "fee_amount":"-0.49388",
//     "tid":11970839,
//     "order_id":446913929     /**************
//   }]
// view how to authenticate here:
// https://docs.bitfinex.com/v1/docs/rest-auth

var payload = {
    request: '/v1/order/status',
    nonce: Date.now().toString(),
      id: 448411153
 }
 
//  {
//     "id":448411153,
//     "symbol":"btcusd",
//     "exchange":null,
//     "price":"0.01",
//     "avg_execution_price":"0.0",   /************************
//     "side":"buy",
//     "type":"exchange limit",
//     "timestamp":"1444276570.0",
//     "is_live":false,           /******************
//     "is_cancelled":true,       /****************
//     "is_hidden":false,
//     "oco_order":null,
//     "was_forced":false,
//     "original_amount":"0.01",    /**********************
//     "remaining_amount":"0.01",   /*********************
//     "executed_amount":"0.0"      /**********************
//   }