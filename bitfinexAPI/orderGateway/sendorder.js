// view how to authenticate here:
// https://docs.bitfinex.com/v1/docs/rest-auth

var payload = {
    request: '/v1/order/new',
    nonce: Date.now().toString(),
    symbol: 'BTCUSD',
    amount: '0.3',
    price: '1000',
    exchange: 'bitfinex',
    side: 'sell',
    type: 'exchange market'
}

 // response
// {
//     "id":448364249,
//     "symbol":"btcusd",
//     "exchange":"bitfinex",
//     "price":"0.01",
//     "avg_execution_price":"0.0",
//     "side":"buy",
//     "type":"exchange limit",
//     "timestamp":"1444272165.252370982",
//     "is_live":true,
//     "is_cancelled":false,
//     "is_hidden":false,
//     "was_forced":false,
//     "original_amount":"0.01",
//     "remaining_amount":"0.01",
//     "executed_amount":"0.0",
//     "order_id":448364249
//   }